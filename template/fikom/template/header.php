<header class="header d-flex flex-row">
    <div class="header_content d-flex flex-row align-items-center">
        <!-- Logo -->
        <div class="logo_container">
            <div class="logo">

                <span style="color:orange;">Logo</span>
            </div>
        </div>

        <!-- Main Navigation -->
        <nav class="main_nav_container">
            <div class="main_nav">
                <ul class="main_nav_list">
                    <li class="main_nav_item"><a href="/">Home</a></li>
                    <li class="main_nav_item"><a href="/about-us">About Us</a></li>
                    <li class="main_nav_item"><a href="/event">Event</a></li>
                    <li class="main_nav_item"><a href="/news">News</a></li>
                    <li class="main_nav_item"><a href="/contact">Contact</a></li>
                </ul>
            </div>
        </nav>
    </div>


    <!-- Hamburger -->
    <div class="hamburger_container">
        <i class="fas fa-bars trans_200"></i>
    </div>

</header>

<div class="menu_container menu_mm">

    <!-- Menu Close Button -->
    <div class="menu_close_container">
        <div class="menu_close"></div>
    </div>

    <!-- Menu Items -->
    <div class="menu_inner menu_mm">
        <div class="menu menu_mm">
            <ul class="menu_list menu_mm">
                <li class="menu_item menu_mm"><a href="/">Home</a></li>
                <li class="menu_item menu_mm"><a href="/about-us">About Us</a></li>
                <li class="menu_item menu_mm"><a href="/event">Event</a></li>
                <li class="menu_item menu_mm"><a href="/news">News</a></li>
                <li class="menu_item menu_mm"><a href="/contact">Contact</a></li>
            </ul>

            <!-- Menu Social -->

            <div class="menu_social_container menu_mm">
                <ul class="menu_social menu_mm">
                    <li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-pinterest"></i></a></li>
                    <li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    <li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-instagram"></i></a></li>
                    <li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-twitter"></i></a></li>
                </ul>
            </div>

            <div class="menu_copyright menu_mm">FikomUP</div>
        </div>

    </div>

</div>